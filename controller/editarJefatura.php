<?php
  ////ini_set('display_errors', 'On');
  require('../model/consultas.php');

  session_start();

  if (count($_POST) > 0) {
    $row = '';

    $rutNuevoJefe = $_POST['rutNuevoJefe'];
    $strlstPersonal = $_POST['strlstPersonal'];

    $strlstPersonal_cambio = str_replace(" ", "", str_replace("'","",$strlstPersonal['ids']));

    $row = actualizarJefe($rutNuevoJefe, $strlstPersonal['ids']);

    if ($row != "Error" ) {
      $row2 = listadoPersonalYJefaturas_Cambio($strlstPersonal_cambio);
      if(is_array($row2))
      {
          $results = array(
              "sEcho" => 1,
              "iTotalRecords" => count($row2),
              "iTotalDisplayRecords" => count($row2),
              "aaData"=>$row2
          );
          echo json_encode($results);
      }
      else{
          $results = array(
              "sEcho" => 1,
              "iTotalRecords" => 0,
              "iTotalDisplayRecords" => 0,
              "aaData"=>[]
          );
          echo json_encode($results);
      }
    } else {
      echo "Sin datos";
    }
  } else {
    echo "Sin datos";
  }
?>
